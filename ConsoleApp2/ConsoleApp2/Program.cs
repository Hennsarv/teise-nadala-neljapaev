﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            byte b = 255;
            b++;
            Console.WriteLine(b);
            unchecked
            {
                int maks = int.MaxValue;
                Console.WriteLine(maks + 1);
            }
        }
    }
}

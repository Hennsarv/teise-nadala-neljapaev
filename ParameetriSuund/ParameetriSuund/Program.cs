﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameetriSuund
{
    class Program
    {
        static void Main(string[] args)
        {
            int v, u=0;
            Arvuta(3, 4, out v, out u);
            Console.WriteLine(v);

            int test = 7;
            Imelik(ref test, ref test);
            Console.WriteLine(test);

        }

        public static void Arvuta(int yks, int kaks, out int summa, out int korrutis)
        {
            summa = yks + kaks;
            korrutis = yks * kaks;
        }

        public static void Arvuta(int[] arvud, out int summa, out double keskmine)
        {
            double sum = 0;
            foreach (var x in arvud) sum += x;
            summa = (int)sum;
            keskmine = sum / arvud.Length;
        }


        public static void Imelik(ref int a, ref int b)
        {
            a++;
            b++;
        }

    }
}

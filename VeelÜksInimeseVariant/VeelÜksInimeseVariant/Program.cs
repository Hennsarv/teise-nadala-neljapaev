﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelÜksInimeseVariant
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Inimene.New("35503070211", "Henn");
               // Inimene.New("35503070211", "Sarvik");
            }
            catch(Exception e) { throw e; }
            finally
            {
                Console.WriteLine($"Inimesi on {Inimene.InimesteArv}");
            }
        }
    }

    class Inimene
    {
        static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();
        public static int InimesteArv => Inimesed.Count;
        // nüüd ülesanne, mis tuleks meil välja mõelda
        // igal inimesel on unikaalne isikukood
        // see antakse inimese loomisel ette
        // teist sama isikukoodiga inimest meil olla ei saa (ei tohi)
        // kui keegi püüab luua sama isikukoodiga, siis peab ta saama
        // juba olemasoleva inimese hoopis
        // ja nüüd tuleks teha meil konstruktor või mis iganes, mis 
        // selliseid inimesi vorpida suudab

            // muutsime ka pisut - kui isikukoodiga on olemas
            // siis ei tee vaid annab vea (throw exception)

        string _Isikukood; public string Isikukood => _Isikukood;
        string _Nimi;
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = Proper(value);
        }

        public Inimene(string isikukood, string nimi)
        {
            _Isikukood = isikukood;
            Nimi = nimi;
            Inimesed.Add(isikukood, this);
        }

        public static Inimene New(string isikukood, string nimi)
        {
            if (!Inimesed.ContainsKey(isikukood))
                return new Inimene(isikukood, nimi);
            else throw new Exception($"Sellise isikukoodiga {isikukood} on juba inimene");

        }

        public static bool TryNew(string isikukood, out Inimene inimene)
        {
            bool vastus = !Inimesed.ContainsKey(isikukood);
            inimene = New(isikukood, "");
            return vastus;
        }

 
        public static string Proper(string tekst)
        {
            return string.Join(" ", 
                tekst
                .Split(' ')
                .Select(x => x == "" ? "":x.Substring(0,1).ToUpper() + x.Substring(1).ToLower())
                );

        }

    }
}

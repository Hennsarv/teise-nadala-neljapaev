﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisJuhtubKui
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0, b = 0;
            while (true)
            {
                
                try
                {
                    Console.WriteLine("anna kaks arvu: ");
                    a = int.Parse(Console.ReadLine());
                    b = int.Parse(Console.ReadLine());
                    Console.WriteLine(a / b);
                    Console.WriteLine("jagasin ja olen õnnelik");
                }
                catch(DivideByZeroException e)
                {
                    Console.WriteLine("ah tahad nulliga jagada");
                }
                catch(FormatException e)
                {
                    Console.WriteLine("ise oled loll");
                }
                catch (Exception e)
                {
                    Console.WriteLine("jagada ei saa");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.GetType().FullName);
                }
                finally
                {
                    Console.WriteLine("aga ma ei jäta");
                }
            }
        }
    }
}
